﻿using CloudWorks.Store.BaseClasses;
using System;

namespace CloudWorks.Store.Classes
{
    public sealed class SelfCheckoutPos : Pos
    {
        public SelfCheckoutPos(int posNumber) : base(posNumber) { }

        public override double Checkout(Client client) =>
            client.CheckoutSpeed * client.Items.Length;

        public override void Print() => Console.WriteLine($"Checkout: {PosNumber}");
    }
}
